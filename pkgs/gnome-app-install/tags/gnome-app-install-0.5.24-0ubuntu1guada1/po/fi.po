# Finnish messages for gnome-app-install
# Copyright (C) 2005 Ilkka Tuohela, Finland.
# This file is distributed under the same license as the gnome-app-install package.
# Ilkka Tuohela <hile@iki.fi>, 2005-2006.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-app-install\n"
"Report-Msgid-Bugs-To: Sebastian Heinlein <sebi@glatzor.de>\n"
"POT-Creation-Date: 2008-01-04 20:30+0100\n"
"PO-Revision-Date: 2007-09-12 17:32+0000\n"
"Last-Translator: Ilkka Tuohela <hile@iki.fi>\n"
"Language-Team: Finnish <gnome-fi-laatu@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2008-01-04 18:56+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#: ../data/gnome-app-install.schemas.in.h:1
msgid "Components that have been confirmed to install applications from"
msgstr "Komponentit jotka on vahvistettu sovellusten asennuslähteiksi"

#: ../data/gnome-app-install.schemas.in.h:2
msgid "If true, the main window is maximized."
msgstr "Jos tosi, pääikkuna on suurennettu koko ruudun kokoiseksi"

#: ../data/gnome-app-install.schemas.in.h:3
msgid "Main window height"
msgstr "Pääikkunan korkeus"

#: ../data/gnome-app-install.schemas.in.h:4
msgid "Main window maximization state"
msgstr "Pääikkunan kokoruututila"

#: ../data/gnome-app-install.schemas.in.h:5
msgid "Main window width"
msgstr "Pääikkunan leveys"

#: ../data/gnome-app-install.schemas.in.h:6
msgid ""
"Possible values: 0 : show all available applications 1 : show only free "
"applications 2 : - 3 : show only supported applications 4 : show only 3rd "
"party applications 5 : - 6 : show only installed applications"
msgstr ""
"Mahdolliset arvot: 0 : näytä kaikki saatavilla olevat sovellukset 1 : näytä "
"vain vapaat sovellukset 2: - 3: näytä vain tuetut sovellukset 4: näytä vain "
"kolmannen osapuolen sovellukset 5: - 6: näytä vain asennetut sovellukset"

#: ../data/gnome-app-install.schemas.in.h:7
msgid "Show only a subset of applications"
msgstr "Näytä vain osa sovelluksista"

#: ../data/gnome-app-install.schemas.in.h:8
msgid "Suggest application for MIME-type: approved components"
msgstr "suosittele sovelluste MIME-tyypille: hyväksytyt komponentit"

#: ../data/gnome-app-install.schemas.in.h:9
msgid "Timeout for interactive search"
msgstr "Aikakatkaisu interaktiivisessa haussa"

#: ../data/gnome-app-install.schemas.in.h:10
msgid ""
"Timeout from typing in the search entry until a search is triggered (in "
"milliseconds)."
msgstr ""
"Hakukenttään kirjoitettaessa aika, jonka jälkeen hakua aletaan suorittaa "
"(millisekunneissa)"

#: ../data/gnome-app-install.schemas.in.h:11
msgid "Timestamp of the last cache refresh run"
msgstr "Viimeisimmän välimuistivirkistyksen aikaleima"

#: ../data/gnome-app-install.schemas.in.h:12
msgid ""
"When the user asks to open a file but no supported application is present, "
"the system will offer to install applications in the components listed here, "
"as well as the individually whitelisted applications."
msgstr ""
"Kun käyttäjä pyytää tiedoston avaamista, mutta tuettua sovellusta ei ole "
"asennettuna, järjestelmä tarjoaa asennettavaa sovellusta näistä "
"komponenteista sekä erityisestä hyväksyttyjen sovellusten (whitelist) "
"luettelosta."

#: ../data/gnome-app-install.glade.h:1
msgid "<b>Add</b>"
msgstr "<b>Lisää</b>"

#: ../data/gnome-app-install.glade.h:2
msgid "<b>Remove</b>"
msgstr "<b>Poista</b>"

#: ../data/gnome-app-install.glade.h:3
msgid ""
"<big><b>Checking installed and available applications</b></big>\n"
"\n"
"Ubuntu and third party vendors offer you a large variety of applications "
"that you can install on your system."
msgstr ""
"<big><b>Tarkistetaan asennettuja ja saatavilla olevia sovelluksia</b></big>\n"
"\n"
"Ubuntu ja kolmannet osapuolet tarjoavat suuren valikoiman järjestelmään "
"asennettavia sovelluksia."

#: ../data/gnome-app-install.glade.h:6
msgid ""
"<big><b>The list of available applications is out of date</b></big>\n"
"\n"
"To reload the list you need a working internet connection."
msgstr ""
"<big><b>Saatavilla olevien ohjelmien luettelo on vanhentunut</b></big>\n"
"\n"
"Ladataksesi luettelon uudelleen tarvitset toimivan Internet-yhteyden."

#: ../data/gnome-app-install.glade.h:9
msgid "Add/Remove Applications"
msgstr "Lisää/poista sovelluksia"

#: ../data/gnome-app-install.glade.h:10
msgid "Apply pending changes and close the window"
msgstr "Toteuta muutokset ja sulje ikkuna"

#: ../data/gnome-app-install.glade.h:11
msgid "Cancel pending changes and close the window"
msgstr "Peruuta muutokset ja sulje ikkuna"

#: ../data/gnome-app-install.glade.h:12
msgid "Categories"
msgstr "Kategoriat"

#: ../data/gnome-app-install.glade.h:13
msgid "Licence"
msgstr "Lisenssi"

#: ../data/gnome-app-install.glade.h:14
msgid "Search:"
msgstr "Etsi:"

#: ../data/gnome-app-install.glade.h:15
msgid "Show:"
msgstr "Näytä:"

#: ../data/gnome-app-install.glade.h:16
msgid "The categories represent the application menu"
msgstr "Kategoriat kuvaavat sovellukset-valikkoa"

#: ../data/gnome-app-install.glade.h:17
msgid "_Add/Remove More Applications"
msgstr "_Lisää/poista muita sovelluksia"

#: ../data/gnome-app-install.glade.h:18
msgid "_Apply Changes"
msgstr "_Toteuta muutokset"

#: ../data/gnome-app-install.glade.h:19
msgid "_Reload"
msgstr "Ta_rkista"

#: ../data/gnome-app-install.glade.h:20
msgid "_Remove"
msgstr "_Poista"

#: ../data/gnome-app-install.glade.h:21
msgid "_Retry"
msgstr "Y_ritä uudelleen"

#: ../data/gnome-app-install.glade.h:22
msgid "applications"
msgstr "sovellukset"

#: ../data/gnome-app-install.desktop.in.h:1
#: ../data/gnome-app-install-xfce.desktop.in.h:1
msgid "Add/Remove..."
msgstr "Lisää/poista..."

#: ../data/gnome-app-install.desktop.in.h:2
#: ../data/gnome-app-install-xfce.desktop.in.h:2
msgid "Install and remove applications"
msgstr "Lisää tai poista sovelluksia"

#: ../data/gnome-app-install.desktop.in.h:3
#: ../data/gnome-app-install-xfce.desktop.in.h:3
msgid "Package Manager"
msgstr "Pakettienhallinta"

#: ../AppInstall/activation.py:112
msgid "no suitable application"
msgstr "ei sopivaa sovellusta"

#: ../AppInstall/activation.py:113
msgid ""
"No application suitable for automatic installation is available for handling "
"this kind of file."
msgstr ""
"Automaattisesti asennettavissa olevaa sovellusta tämän tiedoston avaamiseen "
"ei ole."

#: ../AppInstall/activation.py:116
msgid "no application found"
msgstr "sovellusta ei löytynyt"

#: ../AppInstall/activation.py:117
msgid "No application is known for this kind of file."
msgstr "Tämänlaiselle tiedostolle sopivaa sovellusta ei ole tiedossa."

#: ../AppInstall/activation.py:131 ../AppInstall/activation.py:289
msgid "Searching for appropriate applications"
msgstr "Etsitään sopivia sovelluksia"

#: ../AppInstall/activation.py:133 ../AppInstall/activation.py:192
msgid "Please wait. This might take a minute or two."
msgstr "Odota. Tämä voi kestää jonkin aikaa."

#: ../AppInstall/activation.py:161
msgid "Search for suitable codec?"
msgstr "Etsi sopivaa koodekkia?"

#: ../AppInstall/activation.py:162
msgid ""
"The required software to play this file is not installed. You need to "
"install suitable codecs to play media files. Do you want to search for a "
"codec that supports the selected file?\n"
"\n"
"The search will also include software which is not officially supported."
msgstr ""
"Tämän tiedoston toistamiseen tarvittavia ohjelmistoja ei ole asennettu. "
"Sopivat koodekit tulee asentaa mediatiedostojen toistamiseksi. Etsitäänkö "
"valittua tiedostoa tukevaa koodekkia?\n"
"\n"
"Tämä haku sisältää myös ohjelmia, joita ei virallisesti tueta."

#: ../AppInstall/activation.py:173
msgid "Invalid commandline"
msgstr "Virheellinen komentorivi"

#: ../AppInstall/activation.py:174
#, python-format
msgid "'%s' does not understand the commandline argument '%s'"
msgstr "\"%s\" ei tunnista komentoriviargumenttia \"%s\""

#: ../AppInstall/activation.py:190
msgid "Searching for appropriate codecs"
msgstr "Etsitään sopivia koodekkeja"

#: ../AppInstall/activation.py:195 ../AppInstall/activation.py:294
msgid "_Install"
msgstr "_Asenna"

#: ../AppInstall/activation.py:196
msgid "Install multimedia codecs"
msgstr "Asenna multimediakoodekit"

#: ../AppInstall/activation.py:200
msgid "Codec"
msgstr "Koodekki"

#: ../AppInstall/activation.py:231
msgid "additional codec installation declined"
msgstr "lisäkoodekkien asennuksesta kieltäydytty"

#: ../AppInstall/activation.py:235
msgid "additional codec installation failed"
msgstr "lisäkoodekkien asennus epäonnistui"

#. TRANSLATORS: %s represents a file path
#: ../AppInstall/activation.py:256
#, python-format
msgid "\"%s\" cannot be opened"
msgstr "\"%s\" ei voi avata"

#: ../AppInstall/activation.py:291
#, python-format
msgid ""
"A list of applications that can handle documents of the type '%s' will be "
"created"
msgstr ""
"Asiakirjatyypin \"%s\" käsitttelyn hallitsevista sovelluksista luodaan "
"luettelo"

#. TRANSLATORS: %s represents a file path
#: ../AppInstall/activation.py:297
#, python-format
msgid "Install applications to open \"%s\""
msgstr "Asenna sovellukset kohteen \"%s\" avaamiseksi"

#: ../AppInstall/activation.py:300
msgid "Install applications"
msgstr "Asenna sovellukset"

#: ../AppInstall/activation.py:311
msgid "_Search"
msgstr "Et_si"

#: ../AppInstall/activation.py:345
msgid "Searching for extensions"
msgstr "Etsitään laajennuksia"

#: ../AppInstall/activation.py:346
msgid "Extensions allow you to add new features to your application."
msgstr "Laajennukset lisäävät sovellukseen uusia ominaisuuksia."

#: ../AppInstall/activation.py:348
msgid "Install/Remove Extensions"
msgstr "Asenna/poista laajennuksia"

#: ../AppInstall/activation.py:350
msgid "Extension"
msgstr "Lisäosa"

#: ../AppInstall/AppInstall.py:174
msgid ""
"To install an application check the box next to the application. Uncheck the "
"box to remove the application."
msgstr ""
"Asentaaksesi ohjelman valitse ohjelman vieressä oleva laatikko. Samaten "
"poistaaksesi ohjelman poista valinta."

#: ../AppInstall/AppInstall.py:177
msgid "To perform advanced tasks use the Synaptic package manager."
msgstr ""
"Tehdäksesi edistyneempiä toimenpiteitä käytä Synaptic-"
"pakettienhallintaohjelmaa."

#: ../AppInstall/AppInstall.py:179
msgid "Quick Introduction"
msgstr "Lyhyt esittely"

#: ../AppInstall/AppInstall.py:225
#, fuzzy
msgid "Installed applications only"
msgstr "Asennetut sovellukset"

#: ../AppInstall/AppInstall.py:226
msgid "Show only applications that are installed on your computer"
msgstr "Näytä vain sovellukset, jotka ovat asennettuina tietokoneelle"

#: ../AppInstall/AppInstall.py:318
msgid "Error reading the addon CD"
msgstr "Virhe luettaessa lisä-CD-levyä"

#: ../AppInstall/AppInstall.py:319
msgid "The addon CD may be corrupt "
msgstr "Lisä-CD saattaa olla vioittunut "

#: ../AppInstall/AppInstall.py:453
msgid "The list of applications is not availabe"
msgstr "Sovellusluettelo ei ole saatavissa"

#: ../AppInstall/AppInstall.py:454
msgid ""
"Click on 'Reload' to load it. To reload the list you need a working internet "
"connection. "
msgstr ""
"Napsauta \"Tarkista\" ladataksesi sen. Tämä vaatii toimivan Internet-"
"yhteyden. "

#: ../AppInstall/AppInstall.py:473
#, python-format
msgid "%s cannot be installed on your computer type (%s)"
msgstr "%s ei voida asentaa tietokonetyypillesi (%s)"

#: ../AppInstall/AppInstall.py:476
msgid ""
"Either the application requires special hardware features or the vendor "
"decided to not support your computer type."
msgstr ""
"Sovellus vaatii joko erityistä laitteistoa, tai valmistaja on päättänyt olla "
"tukematta tietokonetyyppiäsi."

#: ../AppInstall/AppInstall.py:517
#, python-format
msgid "Cannot install '%s'"
msgstr "Ei voida asentaa sovellusta '%s'"

#: ../AppInstall/AppInstall.py:518
#, python-format
msgid ""
"This application conflicts with other installed software. To install '%s' "
"the conflicting software must be removed first.\n"
"\n"
"Switch to the 'synaptic' package manager to resolve this conflict."
msgstr ""
"Tämä sovellus on ristiriidassa muiden asennettujen ohjelmistojen kanssa. "
"Asentaaksesi sovelluksen \"%s\" tulee ristiriidassa olevat ohjelmistot "
"poistaa ensin.\n"
"\n"
"Vaihda Synaptic-pakettienhallinnan puolelle korjataksesi tämän ongelman."

#: ../AppInstall/AppInstall.py:579
#, python-format
msgid "Cannot remove '%s'"
msgstr "Ei voida poistaa sovellusta '%s'"

#: ../AppInstall/AppInstall.py:580
#, python-format
msgid ""
"One or more applications depend on %s. To remove %s and the dependent "
"applications, use the Synaptic package manager."
msgstr ""
"Yksi tai useampi sovellus riippuu sovelluksesta %s. Poistaaksesi sovelluksen "
"%s ja siitä riippuvat sovellukset, käytä Synaptic-pakettienhallintaa."

#: ../AppInstall/AppInstall.py:669
msgid "Confirm installation of restricted software"
msgstr "Varmista rajoitettujen ohjelmistojen asentaminen"

#: ../AppInstall/AppInstall.py:670
msgid ""
"The use of this software may be restricted in some countries. You must "
"verify that one of the following is true:\n"
"\n"
"• These restrictions do not apply in your country of legal residence\n"
"• You have permission to use this software (for  example, a patent license)\n"
"• You are using this software for research purposes only"
msgstr ""
"Tämän ohjelmiston käyttö saattaa olla rajoitettua joissain maissa. Yhden "
"seuraavista ehdoista tulee täyttyä:\n"
"\n"
"1. Nämä rajoitukset eivät päde asuinmaassasi\n"
"2. Sinulla on oikeus käyttää tätä ohjelmistoa (esimerkiksi "
"patenttilisenssi)\n"
"3. Käytät tätä ohjelmistoa vain tutkimustarkoituksiin\n"
"\n"
"Epävirallinen huomautus Suomen tilanteesta: Ohjelmistopatentit eivät "
"periaatteessa ole lainvoimaisia Euroopan Unionin alueella, mutta niitä "
"myönnetään kuitenkin jatkuvasti. Tästä johtuen patenttilisenssin tarpeeseen "
"tai tarpeettomuuteen ei ole yksiselitteistä vastausta."

#: ../AppInstall/AppInstall.py:680
msgid "_Confirm"
msgstr "_Varmista"

#: ../AppInstall/AppInstall.py:719 ../AppInstall/DialogProprietary.py:23
#, python-format
msgid "Enable the installation of software from %s?"
msgstr "Ota käyttöön sovellusten asentaminen lähteestä %s?"

#: ../AppInstall/AppInstall.py:721
#, python-format
msgid ""
"%s is provided by a third party vendor. The third party vendor is "
"responsible for support and security updates."
msgstr ""
"Kolmas osapuoli tarjoaa sovelluksen %s. Kyseinen valmistaja on vastuussa "
"tuesta ja tietoturvapäivityksistä."

#: ../AppInstall/AppInstall.py:725 ../AppInstall/DialogProprietary.py:26
msgid "You need a working internet connection to continue."
msgstr "Tarvitset toimivan Internet-yhteyden jatkaaksesi."

#: ../AppInstall/AppInstall.py:729 ../AppInstall/DialogProprietary.py:34
msgid "_Enable"
msgstr "_Ota käyttöön"

#. show an error dialog if something went wrong with the cache
#: ../AppInstall/AppInstall.py:953
msgid "Failed to check for installed and available applications"
msgstr "Ei voitu tarkistaa asennettuja ja saatavilla olevia ohjelmia."

#: ../AppInstall/AppInstall.py:954
msgid ""
"This is a major failure of your software management system. Please check for "
"broken packages with synaptic, check the file permissions and correctness of "
"the file '/etc/apt/sources.list' and reload the software information with: "
"'sudo apt-get update' and 'sudo apt-get install -f'."
msgstr ""
"Tämä on isohko virhe pakettienhallintajärjestelmässä. Tarkista rikkinäiset "
"paketit Synaptic-ohjelmalla, tarkista \"/etc/apt/sources.list\"-tiedoston "
"oikeudet ja sisällön virheettömyys, ja lataa ohjelmaluettelotiedot uudelleen "
"komennoilla:\"sudo apt-get update\" ja \"sudo apt-get install -f\"."

#: ../AppInstall/AppInstall.py:1005
msgid "Apply changes to installed applications before closing?"
msgstr "Toteuta muutokset asennettuihin ohjelmiin ennen sulkemista?"

#: ../AppInstall/AppInstall.py:1006
msgid "If you do not apply your changes they will be lost permanently."
msgstr "Jos et toteuta muutoksia, ne hukataan pysyvästi."

#: ../AppInstall/AppInstall.py:1010
msgid "_Close Without Applying"
msgstr "Sulje _toteuttamatta"

#. FIXME: move this inside the dialog class, we show a different
#. text for a quit dialog and a approve dialog
#: ../AppInstall/AppInstall.py:1071
msgid "Apply the following changes?"
msgstr "Toteutetaanko seuraavat muutokset?"

#: ../AppInstall/AppInstall.py:1072
msgid ""
"Please take a final look through the list of applications that will be "
"installed or removed."
msgstr ""
"Tarkista vielä viimeisen kerran lista sovelluksista, jotka tullaan "
"asentamaan tai poistamaan."

#: ../AppInstall/AppInstall.py:1281
msgid "There is no matching application available."
msgstr "Sovellusta ei löytynyt."

#. TRANSLATORS: %s represents a filter name
#: ../AppInstall/AppInstall.py:1291
#, python-format
msgid "To broaden your search, choose \"%s\"."
msgstr "Laajentaaksesi hakua, valitse \"%s\"."

#. TRANSLATORS: %s represents a filter name
#: ../AppInstall/AppInstall.py:1295
#, python-format
msgid "To broaden your search, choose \"%s\" or \"%s\"."
msgstr "Laajentaaksesi hakua, valitse \"%s\" tai \"%s\"."

#. TRANSLATORS: Show refers to the Show: combobox
#: ../AppInstall/AppInstall.py:1301
msgid "To broaden your search, choose a different \"Show\" item."
msgstr "Laajentaaksesi hakua, valitse \"Näytä\"-pudotusvalikosta jotain muuta"

#. TRANSLATORS: All refers to the All category in the left list
#: ../AppInstall/AppInstall.py:1307
msgid "To broaden your search, choose 'All' categories."
msgstr "Laajentaaksesi hakua valitse \"Kaikki\" vasemman laidan luettelosta."

#: ../AppInstall/BrowserView.py:96
#, python-format
msgid "Failed to open '%s'"
msgstr "Kohdetta \"%s\" ei voi avata"

#: ../AppInstall/CoreMenu.py:127
msgid "Applications"
msgstr "Sovellukset"

#: ../AppInstall/DialogComplete.py:128
msgid "Software installation failed"
msgstr "Ohjelmiston asennus epäonnistui"

#: ../AppInstall/DialogComplete.py:129
msgid ""
"There has been a problem during the installation of the following pieces of "
"software."
msgstr "Seuraavien ohjelmien asennuksessa oli ongelmia."

#: ../AppInstall/DialogComplete.py:131 ../AppInstall/DialogComplete.py:146
#: ../AppInstall/DialogComplete.py:162 ../AppInstall/DialogComplete.py:190
msgid "Add/Remove More Software"
msgstr "Lisää/poista muita ohjelmia"

#: ../AppInstall/DialogComplete.py:133
msgid "Application installation failed"
msgstr "Sovelluksen asentaminen epäonnistui"

#: ../AppInstall/DialogComplete.py:134
msgid ""
"There has been a problem during the installation of the following "
"applications."
msgstr "Seuraavien sovellusten asentamisessa oli ongelmia."

#: ../AppInstall/DialogComplete.py:143
msgid "Software could not be removed"
msgstr "Ohjelmaa ei voitu poistaa"

#: ../AppInstall/DialogComplete.py:144
msgid ""
"There has been a problem during the removal of the following pieces of "
"software."
msgstr "Seuraavien ohjelmien poistamisessa oli ongelmia."

#: ../AppInstall/DialogComplete.py:148
msgid "Not all applications could be removed"
msgstr "Kaikkia sovelluksia ei voitu poistaa"

#: ../AppInstall/DialogComplete.py:149
msgid ""
"There has been a problem during the removal of the following applications."
msgstr "Seuraavien sovellusten poistamisessa oli ongelmia."

#: ../AppInstall/DialogComplete.py:159
msgid "Installation and removal of software failed"
msgstr "Ohjelman asentaminen tai poistaminen epäonnistui"

#: ../AppInstall/DialogComplete.py:160
msgid ""
"There has been a problem during the installation or removal of the following "
"pieces of software."
msgstr "Seuraavien ohjelmien asentamisessa tai poistamisessa oli ongelmia."

#: ../AppInstall/DialogComplete.py:164
msgid "Installation and removal of applications failed"
msgstr "Sovelluksien asennus tai poistaminen epäonnistui"

#: ../AppInstall/DialogComplete.py:165
msgid ""
"There has been a problem during the installation or removal of the following "
"applications."
msgstr "Seuraavien sovellusten asentamisessa tai poistamisessa oli ongelmia."

#: ../AppInstall/DialogComplete.py:173
msgid "New application has been installed"
msgstr "Uusi sovellus on asennettu"

#: ../AppInstall/DialogComplete.py:174
msgid "New applications have been installed"
msgstr "Uusia sovelluksia on asennettu"

#: ../AppInstall/DialogComplete.py:179
msgid ""
"To start a newly installed application, choose it from the applications menu."
msgstr ""
"Käynnistääksesi juuri asennetun sovelluksen, valitse se Sovellukset-"
"valikosta."

#: ../AppInstall/DialogComplete.py:182
msgid "To start a newly installed application double click on it."
msgstr "Käynnistääksesi juuri asennetun sovelluksen, kaksoisnapsauta sitä."

#: ../AppInstall/DialogComplete.py:186
msgid "Software has been installed successfully"
msgstr "Ohjelma on asennettu onnistuneesti"

#: ../AppInstall/DialogComplete.py:187
msgid "Do you want to install or remove further software?"
msgstr "Haluatko asentaa tai poistaa muita ohjelmia?"

#: ../AppInstall/DialogComplete.py:192
msgid "Applications have been removed successfully"
msgstr "Sovellukset on poistettu onnistuneesti"

#: ../AppInstall/DialogComplete.py:193
msgid "Do you want to install or remove further applications?"
msgstr "Haluatko asentaa tai poistaa lisää sovelluksia?"

#: ../AppInstall/DialogMultipleApps.py:33
#, python-format
msgid "Remove %s and bundled applications?"
msgstr "Poista %s ja siihen liittyvät sovellukset?"

#: ../AppInstall/DialogMultipleApps.py:34
#, python-format
msgid ""
"%s is part of a software collection. If you remove %s, you will remove all "
"bundled applications as well."
msgstr ""
"%s on osa ohjelmakokonaisuutta. Jos %s poistetaan, poistetaan myös siihen "
"liittyvät sovellukset."

#: ../AppInstall/DialogMultipleApps.py:37
msgid "_Remove All"
msgstr "_Poista kaikki"

#: ../AppInstall/DialogMultipleApps.py:39
#, python-format
msgid "Install %s and bundled applications?"
msgstr "Asenna %s ja siihen liittyvät sovellukset?"

#: ../AppInstall/DialogMultipleApps.py:40
#, python-format
msgid ""
"%s is part of a software collection. If you install %s, you will install all "
"bundled applications as well."
msgstr ""
"%s on osa ohjelmakokonaisuutta. Jos %s asennetaan, asennetaan myös siihen "
"liittyvät sovellukset."

#: ../AppInstall/DialogMultipleApps.py:43
msgid "_Install All"
msgstr "_Asenna kaikki"

#: ../AppInstall/DialogProprietary.py:25
#, python-format
msgid "%s is provided by a third party vendor."
msgstr "Sovelluksen %s tarjoaa kolmannen osapuolen valmistaja."

#: ../AppInstall/DialogProprietary.py:40
msgid ""
"The application comes with the following license terms and conditions. Click "
"on the 'Enable' button to accept them:"
msgstr ""
"Sovellukseen liittyy seuraavat lisenssiehdot. Napsauta \"Ota käyttöön\" -"
"painiketta hyväksyäksesi ne:"

#: ../AppInstall/DialogProprietary.py:46
msgid "Accept the license terms and install the software"
msgstr "Hyväksy lisenssiehdot ja asenna ohjelma"

#: ../AppInstall/Menu.py:100
msgid "Loading cache..."
msgstr "Luetaan välimuistia..."

#: ../AppInstall/Menu.py:109
msgid "Collecting application data..."
msgstr "Kerätään sovellustietoja..."

#: ../AppInstall/Menu.py:311
msgid "Loading applications..."
msgstr "Luetaan sovelluksia..."

#: ../AppInstall/Menu.py:315 ../AppInstall/Menu.py:318
msgid "All"
msgstr "Kaikki"

#: ../AppInstall/Menu.py:327
#, python-format
msgid "Loading %s..."
msgstr "Luetaan %s..."

#: ../AppInstall/distros/Debian.py:15 ../AppInstall/distros/Default.py:13
#: ../AppInstall/distros/Ubuntu.py:15
msgid "All available applications"
msgstr "Kaikki saatavilla olevat sovellukset"

#: ../AppInstall/distros/Debian.py:16
msgid ""
"Show all applications including ones which are possibly restricted by law or "
"copyright"
msgstr ""
"Näytä kaikki sovellukset, mukaanlukien ne joita mahdollisesti laki tai "
"tekijänoikeudet rajoittavat"

#: ../AppInstall/distros/Debian.py:18 ../AppInstall/distros/Ubuntu.py:20
msgid "All Open Source applications"
msgstr "Kaikki avoimen lähdekoodin sovellukset"

#: ../AppInstall/distros/Debian.py:19
msgid ""
"Show all Debian applications which can be freely used, modified and "
"distributed. This includes a large variety of community maintained "
"applications"
msgstr ""
"Näytä kaikki Debian-sovellukset, joita voidaan vapaasti käyttää, muokata ja "
"jakaa. Tämä sisältää suuren valikoiman yhteisön ylläpitämiä sovelluksia"

#: ../AppInstall/distros/Debian.py:25 ../AppInstall/distros/Ubuntu.py:31
msgid "Third party applications"
msgstr "Kolmannen osapuolen sovellukset"

#: ../AppInstall/distros/Debian.py:26
msgid ""
"Show only applications that are provided by independent software vendors and "
"are not part of Debian"
msgstr ""
"Näytä vain sovellukset, joita tarjoavat itsenäiset ohjelmakehittäjät ja "
"jotka eivät ole osa Debiania"

#. Fallback
#: ../AppInstall/distros/Debian.py:37
#, python-format
msgid "Enable the installation of software from the %s component of Debian?"
msgstr "Ota käyttöön ohjelmien asentaminen Debianin %s-komponentista?"

#. %s is the name of the component
#: ../AppInstall/distros/Debian.py:40 ../AppInstall/distros/Default.py:27
#: ../AppInstall/distros/Ubuntu.py:46
#, python-format
msgid "%s is not officially supported with security updates."
msgstr "Sovellusta %s ei tueta virallisesti tietoturvapäivityksillä."

#: ../AppInstall/distros/Debian.py:42
msgid "Enable the installaion of officially supported Debian software?"
msgstr "Ota käyttöön virallisesti tuettujen Debian-ohjelmien ohjelmalähde?"

#. %s is the name of the application
#: ../AppInstall/distros/Debian.py:45
#, python-format
msgid ""
"%s is part of the Debian main distribution. Debian provides support and "
"security updates, which will be enabled too."
msgstr ""
"%s on osa Debianin \"main\"-jakelua. Debian tarjoaa tuki- ja "
"tietoturvapäivityksiä, jotka otetaan käyttöön myös."

#: ../AppInstall/distros/Debian.py:48
msgid "Enable the installation of partial free software?"
msgstr "Ota käyttöön osittain vapaiden ohjelmien ohjelmalähde?"

#. %s is the name of the application
#: ../AppInstall/distros/Debian.py:50
#, python-format
msgid ""
"%s is not part of the Debian main distribution and requires non-free "
"software to work. Debian provides support and security updates, which will "
"be enabled too."
msgstr ""
"%s ei ole osa Debianin \"main\"-jakelua, ja vaatii toimiakseen epävapaita "
"ohjelmia. Debian tarjoaa tuki- ja tietoturvapäivityksiä, jotka otetaan "
"käyttöön myös."

#: ../AppInstall/distros/Debian.py:54
msgid "Enable the installation of non-free software?"
msgstr "Ota käyttöön epävapaiden ohjelmien ohjelmalähde?"

#. %s is the name of the application
#: ../AppInstall/distros/Debian.py:56 ../AppInstall/distros/Ubuntu.py:63
#: ../AppInstall/distros/Ubuntu.py:114
#, python-format
msgid ""
"The use, modification and distribution of %s is restricted by copyright or "
"by legal terms in some countries."
msgstr ""
"Sovelluksen %s käyttö, muokkaus tai jakelu on rajoitettua joissain maissa."

#. %s is the name of an application
#: ../AppInstall/distros/Debian.py:65
#, python-format
msgid "%s integrates well into the KDE desktop"
msgstr "%s sopii hyvin KDE-työpöytään"

#. %s is the name of an application
#: ../AppInstall/distros/Debian.py:70
#, python-format
msgid "%s integrates well into the GNOME desktop"
msgstr "%s sopii hyvin Gnome-työpöytään"

#. %s is the name of an application
#: ../AppInstall/distros/Debian.py:75
#, python-format
msgid "%s integrates well into the Gnustep desktop"
msgstr "%s sopii hyvin Gnustep-työpöytään"

#. %s is the name of an application
#: ../AppInstall/distros/Debian.py:80
#, python-format
msgid "%s integrates well into the XFCE desktop"
msgstr "%s sopii hyvin XFCE-työpöytään"

#: ../AppInstall/distros/Debian.py:103
#, python-format
msgid "Debian provides support and security updates for %s"
msgstr "Debian tarjoaa tukea ja turvallisuuspäivityksiä ohjelmalle %s"

#: ../AppInstall/distros/Debian.py:108
#, python-format
msgid ""
"%s requires non-free software to work.Debian provides support and security "
"updates."
msgstr ""
"%s vaatii epävapaita ohjelmia toimiakseen. Debian tarjoaa tuki- ja "
"turvallisuuspäivityksiä."

#: ../AppInstall/distros/Debian.py:114
#, python-format
msgid ""
"The use, modification and distribution of %s is restricted by copyright or "
"by legal terms in some countries. Debian provides support and security "
"updates."
msgstr ""
"Ohjelman %s käyttö, muokkaus ja jakaminen on tekijänoikeus- tai lakiasioiden "
"takia rajoitettua joissain maissa. Debian tarjoaa tukea ja "
"turvallisuuspäivityksiä."

#: ../AppInstall/distros/Default.py:14
msgid ""
"Show all applications including ones which are unsupported and possibly "
"restricted by law or copyright"
msgstr ""
"Näytä kaikki sovellukset, mukaanlukien ne joita ei tueta ja jotka ovat "
"mahdollisesti lain tai tekijänoikeuksien rajoittamia"

#. Fallback
#: ../AppInstall/distros/Default.py:25 ../AppInstall/distros/Ubuntu.py:43
#, python-format
msgid "Enable the installation of software from the %s component of Ubuntu?"
msgstr "Ota käyttöön ohjelmien asentaminen Ubuntun %s-komponentista?"

#: ../AppInstall/distros/Ubuntu.py:16
msgid ""
"Show all applications including ones which are possibly restricted by law or "
"copyright, unsupported by Canonical Ltd. or not part of Ubuntu"
msgstr ""
"Näytä kaikki ohjelmat, mukaanlukien ne joita rajoittaa tekijänoikeus- tai "
"lakiasiat, ovat ilman Canonicalin tukea tai eivät ole osa Ubuntua"

#: ../AppInstall/distros/Ubuntu.py:21
msgid ""
"Show all Ubuntu applications which can be freely used, modified and "
"distributed. This includes a large variety of community maintained "
"applications"
msgstr ""
"Näytä kaikki Ubuntun ohjelmat, joita voidaan vapaasti käyttää, muokata ja "
"edelleenjakaa. Tämä sisältää suuren valikoiman yhteisön ylläpitämiä ohjelmia."

#: ../AppInstall/distros/Ubuntu.py:27
msgid "Supported applications"
msgstr "Tuetut sovellukset"

#: ../AppInstall/distros/Ubuntu.py:28
msgid ""
"Show only applications which come with full technical and security support "
"by Canonical Ltd."
msgstr "Näytä vain sovellukset, joita Canonical Ltd. tukee täysin"

#: ../AppInstall/distros/Ubuntu.py:32
msgid ""
"Show only applications that are provided by independent software vendors and "
"are not part of Ubuntu"
msgstr ""
"Näytä vain ohjelmat, joita tarjoavat yksittäiset ohjelmistojen tekijät. "
"Ohjelmat eivät ole osa Ubuntua."

#: ../AppInstall/distros/Ubuntu.py:48
msgid "Enable the installaion of officially supported Ubuntu software?"
msgstr "Ota käyttöön virallisesti tuettujen Ubuntu-ohjelmien ohjelmalähde?"

#. %s is the name of the application
#: ../AppInstall/distros/Ubuntu.py:51
#, python-format
msgid ""
"%s is part of the Ubuntu main distribution. Canonical Ltd. provides support "
"and security updates, which will be enabled too."
msgstr ""
"%s on osa Ubuntun \"main\"-jakelua. Canonical Ltd. tarjoaa tuki- ja "
"turvallisuuspäivityksiä, jotka otetaan käyttöön myös."

#: ../AppInstall/distros/Ubuntu.py:54
msgid "Enable the installation of community maintained software?"
msgstr "Ota käyttöön yhteisön ylläpitämien ohjelmien ohjelmalähde?"

#. %s is the name of the application
#: ../AppInstall/distros/Ubuntu.py:57
#, python-format
msgid ""
"%s is maintained by the Ubuntu community. The Ubuntu community provides "
"support and security updates, which will be enabled too."
msgstr ""
"Ohjelmaa %s ylläpitää Ubuntun yhteisö. Ubuntun yhteisö tarjoaa tuki- ja "
"turvallisuuspäivityksiä, jotka otetaan käyttöön myös."

#: ../AppInstall/distros/Ubuntu.py:60
msgid "Enable the installation of unsupported and restricted software?"
msgstr "Ota käyttöön tukemattomien ja rajoitettujen ohjelmien ohjelmalähde?"

#: ../AppInstall/distros/Ubuntu.py:96
#, python-format
msgid "Canonical Ltd. provides technical support and security updates for %s"
msgstr ""
"Canonical Ltd. tarjoaa teknistä tukea ja turvallisuuspäivityksiä "
"sovellukselle %s"

#: ../AppInstall/distros/Ubuntu.py:109
msgid "This application is provided by the Ubuntu community."
msgstr "Tämän sovelluksen tarjoaa Ubuntun yhteisö"

#: ../AppInstall/widgets/AppDescView.py:16
msgid "Description"
msgstr "Kuvaus"

#: ../AppInstall/widgets/AppDescView.py:75
#, python-format
msgid "%s cannot be installed"
msgstr "Ei voida asentaa sovellusta %s"

#. warn that this app is not available on this plattform
#: ../AppInstall/widgets/AppDescView.py:84
#, python-format
msgid ""
"%s cannot be installed on your computer type (%s). Either the application "
"requires special hardware features or the vendor decided to not support your "
"computer type."
msgstr ""
"Sovellusta %s ei voida asentaa tietokonetyypillesi (%s). Sovelluksella voi "
"olla erityisiä laitteistovaatimuksia, tai sovelluksen valmistaja on "
"päättänyt olla tukematta tietokonetyyppiäsi."

#: ../AppInstall/widgets/AppDescView.py:96
msgid "This application is bundled with the following applications: "
msgstr "Tämä ohjelma jakaa asennuspaketin seuraavien ohjelmien kanssa: "

#: ../AppInstall/widgets/AppDescView.py:196
#, python-format
msgid "Version: %s (%s)"
msgstr "Versio: %s (%s)"

#: ../AppInstall/widgets/AppListView.py:55
msgid "Popularity"
msgstr "Suosio"

#. Application column (icon, name, description)
#: ../AppInstall/widgets/AppListView.py:78
msgid "Application"
msgstr "Sovellus"

#~ msgid "%s integrates well into the Kubuntu desktop"
#~ msgstr "%s sopii hyvin Kubuntu-työpöytään"

#~ msgid "%s integrates well into the Ubuntu desktop"
#~ msgstr "%s sopii hyvin Ubuntu-työpöytään"

#~ msgid "%s integrates well into the Xubuntu desktop"
#~ msgstr "%s sopii hyvin Xubuntu-työpöytään"
