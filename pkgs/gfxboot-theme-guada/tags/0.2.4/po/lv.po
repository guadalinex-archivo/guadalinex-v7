# Latvian translation for gfxboot-theme-ubuntu
# Copyright (c) (c) 2006 Canonical Ltd, and Rosetta Contributors 2006
# This file is distributed under the same license as the gfxboot-theme-ubuntu package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2006.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gfxboot-theme-ubuntu\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2006-03-18 12:14:59.394968+00:00\n"
"PO-Revision-Date: 2006-07-24 08:05+0000\n"
"Last-Translator: mixat <miks.latvis@gmail.com>\n"
"Language-Team: Latvian <lv@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. ok button label
#. txt_ok
msgid "OK"
msgstr "Labi"

#. cancel button label
#. txt_cancel
msgid "Cancel"
msgstr "Atsaukt"

#. reboot button label
#. txt_reboot
msgid "Reboot"
msgstr "Pārstartēt"

#. continue button label
#. txt_continue
msgid "Continue"
msgstr "Turpināt"

#. txt_bootoptions
msgid "Boot Options"
msgstr "Sāknēšanas Parametri"

#. window title for exit dialog
#. txt_exit_title (see txt_exit_dialog)
msgid "Exiting..."
msgstr "Izeju..."

#. txt_exit_dialog
msgid ""
"You are leaving the graphical boot menu and\n"
"starting the text mode interface."
msgstr ""
"Jūs atstājāt grafisko sāknēšanas izvēlni un\n"
"palaidāt teksta režīma interfeisu."

#. txt_help
msgid "Help"
msgstr "Palīgs"

#. window title for kernel loading (see txt_load_kernel)
#. txt_load_kernel_title
msgid "Starting..."
msgstr "Palaiž..."

#. Keep the three newlines!
#. txt_load_kernel
msgid ""
"Loading Linux Kernel\n"
"\n"
"\n"
msgstr ""
"Lādē Linux kodolu\n"
"\n"
"\n"

#. Keep the three newlines!
#. txt_load_memtest
msgid ""
"Loading memtest86\n"
"\n"
"\n"
msgstr ""
"Lādēju memtest86\n"
"\n"
"\n"

#. info box title
#. txt_info_title
msgid "Boot loader"
msgstr "Sāknētājs"

#. error box title
#. txt_error_title
msgid "I/O error"
msgstr "I/O kļūda"

#. boot disk change dialog title
#. txt_change_disk_title
msgid "Change Boot Disk"
msgstr "Nomainīt lādēšanas disu"

#. txt_insert_disk
#, c-format
msgid "Insert boot disk %u."
msgstr "Ievietojiet lādēšanas disku %u"

#. txt_insert_disk2
#, c-format
msgid ""
"This is boot disk %u.\n"
"Insert boot disk %u."
msgstr ""
"Tas ir lādēšanas disks %u.\n"
"Ievietojiet lādēšanas disku %u."

#. txt_insert_disk3
#, c-format
msgid ""
"This is not a suitable boot disk.\n"
"Please insert boot disk %u."
msgstr ""
"Šis lādēšanas disks nav derīgs.\n"
"Lūdzu ievietojiet lādēšanas disku %u."

#. password dialog title
#. txt_password_title
msgid "Password"
msgstr "Parole"

#. Keep the newlines and spaces after ':'!
#. txt_password
msgid ""
"Enter your password:   \n"
"\n"
"\n"
msgstr ""
"Ievadiet jūsu paroli:   \n"
"\n"
"\n"

#. dvd warning title
#. txt_dvd_warning_title
msgid "DVD Error"
msgstr "DVD Kļūda"

#. txt_dvd_warning2
msgid ""
"This is a two-sided DVD. You have booted from the second side.\n"
"\n"
"Turn the DVD over then continue."
msgstr ""
"Tas ir divpusējs DVD disks. Jūs ielādējāties no otras puses.\n"
"Apgrieziet DVD disku otrādi un turpiniet."

#. power off dialog title
#. txt_power_off_title
msgid "Power Off"
msgstr "Izslēgt datoru"

#. txt_power_off
msgid "Halt the system now?"
msgstr "Apturēt sistēmu tagad?"

#. dialog title for hard disk installation
#. txt_harddisk_title
msgid "Hard Disk Installation"
msgstr "Instalācija uz cietā diska"

#. txt_hd_diskdevice
msgid "Disk Device (scan all disks if empty)\n"
msgstr "Diskierīce (skanēt visus diskus, ja tukšs)\n"

#. txt_directory
msgid "Directory\n"
msgstr "Mape\n"

#. dialog title for ftp installation
#. txt_ftp_title
msgid "FTP Installation"
msgstr "Instalācija caur FTP"

#. txt_server
msgid "Server\n"
msgstr "Serveris\n"

#. txt_password
msgid "Password\n"
msgstr "Parole\n"

#. label for ftp user input
#. txt_user1
msgid "User (anonymous login if empty)\n"
msgstr "Lietotājs (lietot anonīmu ieeju, ja tukšs)\n"

#. dialog title for nfs installation
#. txt_nfs_title
msgid "NFS Installation"
msgstr "Instalācija caur NFS"

#. label for smb user input
#. txt_user2
msgid "User (using \"guest\" if empty)\n"
msgstr ""

#. dialog title for smb installation
#. txt_smb_title
msgid "SMB (Windows Share) Installation"
msgstr ""

#. dialog title for http installation
#. txt_http_title
msgid "HTTP Installation"
msgstr "instalācija caur HTTP"

#. as in Windows Authentication Domain
#. txt_domain
msgid "Domain\n"
msgstr "Domēns\n"

#. button label for other/more options
#. txt_other_options
msgid "Other Options"
msgstr "Citi Parametri"

#. label for language selection
#. txt_language
msgid "Language"
msgstr "Valoda"

#. label for keymap selection
#. txt_keymap
msgid "Keymap"
msgstr "Taustiņkarte"

#. label for d-i mode menu
#. txt_normal_mode
msgid "Normal mode"
msgstr "Parastais režīms"

#. label for d-i mode menu
#. txt_expert_mode
msgid "Expert mode"
msgstr "Eksperta režīms"

#. title for accessibility menu
#. txt_access
msgid "Accessibility"
msgstr "Pieejamība"

#. label for accessibility menu
#. txt_access_none
msgid "None"
msgstr "Nav"

#. label for accessibility menu
#. txt_access_v1
msgid "High Contrast"
msgstr ""

#. label for accessibility menu
#. txt_access_v2
msgid "Magnifier"
msgstr ""

#. label for accessibility menu
#. txt_access_v3
msgid "Screen Reader"
msgstr ""

#. label for accessibility menu
#. txt_access_m1
msgid "Keyboard Modifiers"
msgstr ""

#. label for accessibility menu
#. txt_access_m2
msgid "On-Screen Keyboard"
msgstr ""

#. label for accessibility menu
#. txt_access_m3
msgid "Motor Difficulties - switch devices"
msgstr "Dzinēja funkciju problēmas - pārslēgšanas ierīces"

#. label for accessibility menu
#. txt_access_all
msgid "Everything"
msgstr "Viss"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu
msgid "^Start or install Ubuntu"
msgstr "^Palaist vai instalēt Ubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_xforcevesa
msgid "Start Ubuntu in safe ^graphics mode"
msgstr "Sākt Ubuntu drošajā ^grafiskajā režīmā"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_kubuntu
msgid "^Start or install Kubuntu"
msgstr "^Sākt vai instalēt Kubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_kubuntu_xforcevesa
msgid "Start Kubuntu in safe ^graphics mode"
msgstr "Sākt Kubuntu drošajā ^grafiskajā režīmā"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_edubuntu
msgid "^Start or install Edubuntu"
msgstr "^Sākt vai instalēt Edubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_edubuntu_xforcevesa
msgid "Start Edubuntu in safe ^graphics mode"
msgstr "Sākt Edubuntu drošajā ^grafiskajā režīmā"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_xubuntu
msgid "^Start or install Xubuntu"
msgstr "^Sākt vai instalēt Xubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_xubuntu_xforcevesa
msgid "Start Xubuntu in safe ^graphics mode"
msgstr "Sākt Xubuntu drošajā ^grafiskajā režīmā"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text
msgid "^Install in text mode"
msgstr "^Instalēt teksta režīmā"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install
msgid "^Install to the hard disk"
msgstr "^Instalēt uz cietā diska"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_workstation
msgid "Install a ^workstation"
msgstr "Uzstādīt ^darba staciju"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_oem
msgid "Install in ^OEM mode"
msgstr "Uzstādīt ^OEM režīmā"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_lamp
msgid "Install a ^LAMP server"
msgstr "Uzstādīt ^LAMP serveri"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_cli
#, fuzzy
msgid "Install a comm^and-line system"
msgstr "Uzstādīt ^serveri"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_check
msgid "^Check CD for defects"
msgstr "Pārbaudīt CD pret de^fektiem"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_rescue
msgid "^Rescue a broken system"
msgstr "^Atjaunot bojātu sistēmu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_memtest
msgid "^Memory test"
msgstr "^Atmiņas tests"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_hd
msgid "^Boot from first hard disk"
msgstr "Lādē^ties no pirmā cietā diska"

#~ msgid "Blindness"
#~ msgstr "Aklums"
