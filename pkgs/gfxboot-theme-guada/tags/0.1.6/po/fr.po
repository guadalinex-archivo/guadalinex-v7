# translation of fr.po to 
# translation of fr.po to français
# translation of bootloader.fr.po to Français
# translation of bootloader.po to Français
# LANGUAGE translations for boot loader
# Copyright (C) 2004 SUSE LINUX AG
# Patricia Vaz <Patricia.Vaz@suse.de>, 2004.
# Patricia Vaz <patricia.vaz@suse.com>, 2004.
# Patricia Vaz <patricia@suse.de>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: fr\n"
"PO-Revision-Date: 2005-03-07 09:49+0100\n"
"Last-Translator: Novell Language <language@novell.com>\n"
"Language-Team: Novell Language <language@novell.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9\n"

# ok button label
#. txt_ok
msgid "OK"
msgstr "OK"

# cancel button label
#. txt_cancel
msgid "Cancel"
msgstr "Annuler"

# reboot button label
#. txt_reboot
msgid "Reboot"
msgstr "Réamorcer"

# continue button label
#. txt_continue
msgid "Continue"
msgstr "Continuer"

#. txt_bootoptions
msgid "Boot Options"
msgstr "Options d'amorçage"

# window title for exit dialog
#. txt_exit_title (see txt_exit_dialog)
msgid "Exiting..."
msgstr "Sortie..."

#. txt_exit_dialog
msgid ""
"You are leaving the graphical boot menu and\n"
"starting the text mode interface."
msgstr ""
"Vous quittez le menu d'amorçage graphique et\n"
"démarrez l'interface en mode textuel."

#. txt_help
msgid "Help"
msgstr "Aide"

# window title for kernel loading (see txt_load_kernel)
#. txt_load_kernel_title
msgid "Starting..."
msgstr "Démarrage..."

# Keep the three newlines!
#. txt_load_kernel
msgid ""
"Loading Linux Kernel\n"
"\n"
"\n"
msgstr ""
"Chargement du noyau Linux\n"
"\n"
"\n"

# Keep the three newlines!
#. txt_load_memtest
msgid ""
"Loading memtest86\n"
"\n"
"\n"
msgstr ""
"Chargement de memtest86\n"
"\n"
"\n"

# info box title
#. txt_info_title
msgid "Boot loader"
msgstr "Chargeur d'amorçage"

# error box title
#. txt_error_title
msgid "I/O error"
msgstr "Erreur E/S"

# boot disk change dialog title
#. txt_change_disk_title
msgid "Change Boot Disk"
msgstr "Changer la disquette d'amorçage"

#. txt_insert_disk
#, c-format
msgid "Insert boot disk %u."
msgstr "Insérer le disque d'amorçage %u."

#. txt_insert_disk2
#, c-format
msgid ""
"This is boot disk %u.\n"
"Insert boot disk %u."
msgstr ""
"Il s'agit du disque d'amorçage %u.\n"
"Insérer le disque d'amorçage %u."

# <product> is e.g. SuSE Linux X.Y or Enterprise Server Z
#. txt_insert_disk3
#, fuzzy, c-format
msgid ""
"This is not a suitable boot disk.\n"
"Please insert boot disk %u."
msgstr ""
"Ce n'est pas un disque d'amorçage <product>.\n"
"Insérez le disque d'amorçage %u."

# password dialog title
#. txt_password_title
msgid "Password"
msgstr "Mot de passe"

# Keep the newlines and spaces after ':'!
#. txt_password
msgid ""
"Enter your password:   \n"
"\n"
"\n"
msgstr ""
"Entrez votre mot de passe :   \n"
"\n"
"\n"

# dvd warning title
#. txt_dvd_warning_title
msgid "DVD Error"
msgstr "Erreur DVD"

#. txt_dvd_warning2
msgid ""
"This is a two-sided DVD. You have booted from the second side.\n"
"\n"
"Turn the DVD over then continue."
msgstr ""
"Il s'agit d'un DVD double face. Vous avez effectué l'amorçage à partir de la "
"seconde face.\n"
"\n"
"Retournez le DVD, puis continuez."

# power off dialog title
#. txt_power_off_title
msgid "Power Off"
msgstr "Arrêt"

#. txt_power_off
msgid "Halt the system now?"
msgstr "Arrêter le système maintenant ?"

# dialog title for hard disk installation
#. txt_harddisk_title
msgid "Hard Disk Installation"
msgstr "Installation du disque dur"

#. txt_hd_diskdevice
msgid "Disk Device (scan all disks if empty)\n"
msgstr "Périphérique disque (balayer tous les disques si vide)\n"

#. txt_directory
msgid "Directory\n"
msgstr "Répertoire\n"

# dialog title for ftp installation
#. txt_ftp_title
msgid "FTP Installation"
msgstr "Installation FTP"

#. txt_server
msgid "Server\n"
msgstr "Serveur\n"

#. txt_password
msgid "Password\n"
msgstr "Mot de passe\n"

# label for ftp user input
#. txt_user1
msgid "User (anonymous login if empty)\n"
msgstr "Utilisateur (login anonyme si vide)\n"

# dialog title for nfs installation
#. txt_nfs_title
msgid "NFS Installation"
msgstr "Installation NFS"

# label for smb user input
#. txt_user2
msgid "User (using \"guest\" if empty)\n"
msgstr "Utilisateur (utilisation de \"guest\" si vide)\n"

# dialog title for smb installation
#. txt_smb_title
msgid "SMB (Windows Share) Installation"
msgstr "Installation SMB (partage Windows)"

# dialog title for http installation
#. txt_http_title
msgid "HTTP Installation"
msgstr "Installation HTTP"

# as in Windows Authentication Domain
#. txt_domain
msgid "Domain\n"
msgstr "Domaine\n"

# button label for other/more options
#. txt_other_options
msgid "Other Options"
msgstr "Autres options"

# label for language selection
#. txt_language
msgid "Language"
msgstr "Langue"

# label for d-i mode menu
#. txt_normal_mode
msgid "Normal mode"
msgstr ""

# label for d-i mode menu
#. txt_expert_mode
msgid "Expert mode"
msgstr ""
