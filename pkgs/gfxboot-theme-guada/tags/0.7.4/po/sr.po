# Serbian translations for boot loader
# Copyright (C) 2005 SUSE Linux GmbH
# Copyright © 2005 Danilo Segan <danilo@gnome.org>
#
msgid ""
msgstr ""
"Project-Id-Version: bootloader\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-03-27 22:16+0000\n"
"PO-Revision-Date: 2008-03-17 13:37+0000\n"
"Last-Translator: Љубиша Радовановић <prevod@gmail.com>\n"
"Language-Team: Serbian <novell@prevod.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2009-04-03 09:04+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#. ok button label
#. txt_ok
msgid "OK"
msgstr "У реду"

#. cancel button label
#. txt_cancel
msgid "Cancel"
msgstr "Одустани"

#. reboot button label
#. txt_reboot
msgid "Reboot"
msgstr "Ресетуј"

#. continue button label
#. txt_continue
msgid "Continue"
msgstr "Настави"

#. txt_bootoptions
msgid "Boot Options"
msgstr "Опције за покретање"

#. window title for exit dialog
#. txt_exit_title (see txt_exit_dialog)
msgid "Exiting..."
msgstr "Завршавам..."

#. txt_exit_dialog
msgid ""
"You are leaving the graphical boot menu and\n"
"starting the text mode interface."
msgstr ""
"Напуштате графички мени за покретање и\n"
"покрећете текстуално сучеље."

#. txt_help
msgid "Help"
msgstr "Помоћ"

#. info box title
#. txt_info_title
msgid "Boot loader"
msgstr "Покретач система"

#. error box title
#. txt_error_title
msgid "I/O error"
msgstr "У/И грешка"

#. boot disk change dialog title
#. txt_change_disk_title
msgid "Change Boot Disk"
msgstr "Промените диск за покретање"

#. txt_insert_disk
#, c-format
msgid "Insert boot disk %u."
msgstr "Уметните диск за покретање %u."

#. txt_insert_disk2
#, c-format
msgid ""
"This is boot disk %u.\n"
"Insert boot disk %u."
msgstr ""
"Ово је диск за покретање %u.\n"
"Уметните диск за покретање %u."

#. txt_insert_disk3
#, c-format
msgid ""
"This is not a suitable boot disk.\n"
"Please insert boot disk %u."
msgstr ""
"Ово није адекватан диск за покретање <product>.\n"
"Уметните диск за покретање %u."

#. password dialog title
#. txt_password_title
msgid "Password"
msgstr "Лозинка"

#. Keep the newlines and spaces after ':'!
#. txt_password
msgid ""
"Enter your password:   \n"
"\n"
"\n"
msgstr ""
"Унесите своју лозинку:   \n"
"\n"
"\n"

#. dvd warning title
#. txt_dvd_warning_title
msgid "DVD Error"
msgstr "ДВД грешка"

#. txt_dvd_warning2
msgid ""
"This is a two-sided DVD. You have booted from the second side.\n"
"\n"
"Turn the DVD over then continue."
msgstr ""
"Ово је двострани ДВД.  Покренули сте са друге стране.\n"
"\n"
"Окрените ДВД и затим наставите."

#. power off dialog title
#. txt_power_off_title
msgid "Power Off"
msgstr "Угаси"

#. txt_power_off
msgid "Halt the system now?"
msgstr "Сада угасити систем?"

#. txt_password
msgid "Password\n"
msgstr "Лозинка\n"

#. button label for other/more options
#. txt_other_options
msgid "Other Options"
msgstr "Остале опције"

#. label for language selection
#. txt_language
msgid "Language"
msgstr "Језик"

#. label for keymap selection
#. txt_keymap
msgid "Keymap"
msgstr "Мапа тастера"

#. label for installation mode selection
#. txt_modes
msgid "Modes"
msgstr "Начини"

#. Shown on main menu to point out that more boot options are on F4.
#. txt_modes_help
msgid "Press F4 to select alternative start-up and installation modes."
msgstr "Притисните F4 да изаберете алтернативни улаз и начине инсталације."

#. label for modes menu
#. txt_mode_normal
msgid "Normal"
msgstr ""

#. label for d-i mode menu
#. txt_expert_mode
msgid "Expert mode"
msgstr "Напредни режим"

#. title for accessibility menu
#. txt_access
msgid "Accessibility"
msgstr "Приступачност"

#. label for accessibility menu
#. txt_access_none
msgid "None"
msgstr "ништа"

#. label for accessibility menu
#. txt_access_v1
msgid "High Contrast"
msgstr "Велики контраст"

#. label for accessibility menu
#. txt_access_v2
msgid "Magnifier"
msgstr "Лупа"

#. label for accessibility menu
#. txt_access_v3
msgid "Screen Reader"
msgstr "Читач екрана"

#. label for accessibility menu
#. txt_access_brltty
msgid "Braille Terminal"
msgstr "Брејев терминал"

#. label for accessibility menu
#. txt_access_m1
msgid "Keyboard Modifiers"
msgstr "Модификатори тастатуре"

#. label for accessibility menu
#. txt_access_m2
msgid "On-Screen Keyboard"
msgstr "тастатура на екрану"

#. label for accessibility menu
#. txt_access_m3
msgid "Motor Difficulties - switch devices"
msgstr "Проблеми са мотором - промените уређај"

#. label for accessibility menu
#. txt_access_all
msgid "Everything"
msgstr "Све"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu
msgid "^Try Ubuntu without any change to your computer"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_kubuntu
msgid "^Try Kubuntu without any change to your computer"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_edubuntu
msgid "^Try Edubuntu without any change to your computer"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_xubuntu
msgid "^Try Xubuntu without any change to your computer"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_mid
msgid "^Try Ubuntu MID without any change to your computer"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_netbook_remix
msgid "^Try Ubuntu Netbook Remix without any change to your computer"
msgstr ""

#. Installation mode.
#. txt_menuitem_xforcevesa
msgid "Safe graphics mode"
msgstr ""

#. Installation mode.
#. txt_menuitem_driverupdates
msgid "Use driver update disc"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_ubuntu
msgid "^Install Ubuntu in text mode"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_kubuntu
msgid "^Install Kubuntu in text mode"
msgstr "^Инсталирај Kubuntu у текстуалном моду"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_edubuntu
msgid "^Install Edubuntu in text mode"
msgstr "^Инсталирај Edubuntu у текстуалном моду"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_xubuntu
msgid "^Install Xubuntu in text mode"
msgstr "^Инсталирај Xubuntu у текстуалном моду"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu
msgid "^Install Ubuntu"
msgstr "^Инсталирај Ubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_kubuntu
msgid "^Install Kubuntu"
msgstr "^Инсталирај Kubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_edubuntu
msgid "^Install Edubuntu"
msgstr "^Инсталирај Edubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_xubuntu
msgid "^Install Xubuntu"
msgstr "^Инсталирај Xubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_server
msgid "^Install Ubuntu Server"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntustudio
msgid "^Install Ubuntu Studio"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_mid
msgid "^Install Ubuntu MID"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_netbook_remix
msgid "^Install Ubuntu Netbook Remix"
msgstr ""

#. Installation mode.
#. txt_menuitem_workstation
msgid "Install a workstation"
msgstr ""

#. Installation mode.
#. txt_menuitem_server
msgid "Install a server"
msgstr ""

#. Installation mode.
#. txt_menuitem_oem
msgid "OEM install (for manufacturers)"
msgstr ""

#. Installation mode.
#. txt_menuitem_lamp
msgid "Install a LAMP server"
msgstr ""

#. Installation mode.
#. txt_menuitem_ltsp
msgid "Install an LTSP server"
msgstr ""

#. Installation mode.
#. txt_menuitem_ltsp_mythbuntu
msgid "Install a Diskless Image Server"
msgstr "Инсталирај Сервер слику без диска"

#. Installation mode.
#. txt_menuitem_cli
msgid "Install a command-line system"
msgstr ""

#. Installation mode.
#. txt_menuitem_minimal
msgid "Install a minimal system"
msgstr ""

#. Installation mode.
#. txt_menuitem_minimalvm
msgid "Install a minimal virtual machine"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_check
msgid "^Check disc for defects"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_rescue
msgid "^Rescue a broken system"
msgstr "^Спаси оштећен систем"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_memtest
msgid "Test ^memory"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_hd
msgid "^Boot from first hard disk"
msgstr "^Покрени са првог фиксногдиска"

#. Boot option.
#. txt_option_free
msgid "Free software only"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_dell_factory_recovery
msgid "^Dell Automatic Reinstall"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_mythbuntu
msgid "^Install Mythbuntu"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_mythbuntu
msgid "^Try Mythbuntu without any change to your computer"
msgstr ""
