# LITHUANIAN translations for boot loader
# Copyright (C) 2005 SUSE Linux GmbH
#
msgid ""
msgstr ""
"Project-Id-Version: bootloader\n"
"Report-Msgid-Bugs-To:  \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2007-08-28 15:07+0000\n"
"Last-Translator: Romas Mažeika <romas@mazeika.lt>\n"
"Language-Team: Lithuanian <i18n@operis.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2007-10-09 09:00+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#. ok button label
#. txt_ok
msgid "OK"
msgstr "Gerai"

#. cancel button label
#. txt_cancel
msgid "Cancel"
msgstr "Atšaukti"

#. reboot button label
#. txt_reboot
msgid "Reboot"
msgstr "Perkrauti"

#. continue button label
#. txt_continue
msgid "Continue"
msgstr "Tęsti"

#. txt_bootoptions
msgid "Boot Options"
msgstr "Įkrovos parametrai"

#. window title for exit dialog
#. txt_exit_title (see txt_exit_dialog)
msgid "Exiting..."
msgstr "Išeinama..."

#. txt_exit_dialog
msgid ""
"You are leaving the graphical boot menu and\n"
"starting the text mode interface."
msgstr ""
"Jūs paliekate grafinį įkrovos meniu ir\n"
"paleidžiate tekstinę sąsają."

#. txt_help
msgid "Help"
msgstr "Pagalba"

#. window title for kernel loading (see txt_load_kernel)
#. txt_load_kernel_title
msgid "Starting..."
msgstr "Paleidžiama..."

#. Keep the three newlines!
#. txt_load_kernel
msgid ""
"Loading Linux Kernel\n"
"\n"
"\n"
msgstr ""
"Įkeliamas Linux branduolys\n"
"\n"
"\n"

#. Keep the three newlines!
#. txt_load_memtest
msgid ""
"Loading memtest86\n"
"\n"
"\n"
msgstr ""
"Įkeliamas memtest86\n"
"\n"
"\n"

#. info box title
#. txt_info_title
msgid "Boot loader"
msgstr "Sistemų įkėliklis"

#. error box title
#. txt_error_title
msgid "I/O error"
msgstr "I/O klaida"

#. boot disk change dialog title
#. txt_change_disk_title
msgid "Change Boot Disk"
msgstr "Keisti įkrovos diską"

#. txt_insert_disk
#, c-format
msgid "Insert boot disk %u."
msgstr "Įdėkite įkrovos diską %u."

#. txt_insert_disk2
#, c-format
msgid ""
"This is boot disk %u.\n"
"Insert boot disk %u."
msgstr ""
"Tai įkrovos diskas %u.\n"
"Įdėkite įkrovos diską %u."

#. txt_insert_disk3
#, c-format
msgid ""
"This is not a suitable boot disk.\n"
"Please insert boot disk %u."
msgstr ""
"Tai netinkamas įkrovos diskas.\n"
"Įdėkite įkrovos diską %u."

#. password dialog title
#. txt_password_title
msgid "Password"
msgstr "Slaptažodis"

#. Keep the newlines and spaces after ':'!
#. txt_password
msgid ""
"Enter your password:   \n"
"\n"
"\n"
msgstr ""
"Įveskite savo slaptažodį:   \n"
"\n"
"\n"

#. dvd warning title
#. txt_dvd_warning_title
msgid "DVD Error"
msgstr "DVD klaida"

#. txt_dvd_warning2
msgid ""
"This is a two-sided DVD. You have booted from the second side.\n"
"\n"
"Turn the DVD over then continue."
msgstr ""
"Tai dvipusis DVD. Jūs įkrovėte iš antrosios pusės.\n"
"\n"
"Apverskite DVD ir tęskite."

#. power off dialog title
#. txt_power_off_title
msgid "Power Off"
msgstr "Išjungti"

#. txt_power_off
msgid "Halt the system now?"
msgstr "Sustabdyti sistemą?"

#. dialog title for hard disk installation
#. txt_harddisk_title
msgid "Hard Disk Installation"
msgstr "Diegimas iš kietojo disko"

#. txt_hd_diskdevice
msgid "Disk Device (scan all disks if empty)\n"
msgstr "Diskų įrenginys (jei nėra, skenuoti visus diskus)\n"

#. txt_directory
msgid "Directory\n"
msgstr "Katalogas\n"

#. dialog title for ftp installation
#. txt_ftp_title
msgid "FTP Installation"
msgstr "Diegimas iš FTP"

#. txt_server
msgid "Server\n"
msgstr "Serveris\n"

#. txt_password
msgid "Password\n"
msgstr "Slaptažodis\n"

#. label for ftp user input
#. txt_user1
msgid "User (anonymous login if empty)\n"
msgstr "Vartotojas (jei tuščia, jungiasi anonimas)\n"

#. dialog title for nfs installation
#. txt_nfs_title
msgid "NFS Installation"
msgstr "Diegimas per NFS"

#. label for smb user input
#. txt_user2
msgid "User (using \"guest\" if empty)\n"
msgstr "Vartotojas (jei tuščia, naudojama „guest“)\n"

#. dialog title for smb installation
#. txt_smb_title
msgid "SMB (Windows Share) Installation"
msgstr "Diegimas per SMB (Windows Share)"

#. dialog title for http installation
#. txt_http_title
msgid "HTTP Installation"
msgstr "Diegimas per HTTP"

#. as in Windows Authentication Domain
#. txt_domain
msgid "Domain\n"
msgstr "Domenas\n"

#. button label for other/more options
#. txt_other_options
msgid "Other Options"
msgstr "Kiti parametrai"

#. label for language selection
#. txt_language
msgid "Language"
msgstr "Kalba/Lang."

#. label for keymap selection
#. txt_keymap
msgid "Keymap"
msgstr "Klaviatūra"

#. label for d-i mode menu
#. txt_normal_mode
msgid "Normal mode"
msgstr "Normalus režimas"

#. label for d-i mode menu
#. txt_expert_mode
msgid "Expert mode"
msgstr "Eksperto režimas"

#. title for accessibility menu
#. txt_access
msgid "Accessibility"
msgstr "Prieinamumas"

#. label for accessibility menu
#. txt_access_none
msgid "None"
msgstr "Nėra"

#. label for accessibility menu
#. txt_access_v1
msgid "High Contrast"
msgstr "Labai kontrastinga"

#. label for accessibility menu
#. txt_access_v2
msgid "Magnifier"
msgstr "Lupa"

#. label for accessibility menu
#. txt_access_v3
msgid "Screen Reader"
msgstr "Ekrano skaitymas balsu"

#. label for accessibility menu
#. txt_access_brltty
msgid "Braille Terminal"
msgstr "Brailio terminalas"

#. label for accessibility menu
#. txt_access_m1
#, fuzzy
msgid "Keyboard Modifiers"
msgstr "Klaviatūros keitikliai"

#. label for accessibility menu
#. txt_access_m2
msgid "On-Screen Keyboard"
msgstr "Vaizdinė klaviatūra"

#. label for accessibility menu
#. txt_access_m3
msgid "Motor Difficulties - switch devices"
msgstr "Motorikos sutrikimai - perjungti įrenginius"

#. label for accessibility menu
#. txt_access_all
msgid "Everything"
msgstr "Viską"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu
msgid "^Start or install Ubuntu"
msgstr "^Paleisti ar įdiegti Ubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_xforcevesa
msgid "Start Ubuntu in safe ^graphics mode"
msgstr "Paleisti Ubuntu VESA ^grafiniu režimu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_kubuntu
msgid "^Start or install Kubuntu"
msgstr "^Paleisti ar įdiegti Kubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_kubuntu_xforcevesa
msgid "Start Kubuntu in safe ^graphics mode"
msgstr "Paleisti Kubuntu VESA ^grafiniu režimu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_edubuntu
msgid "^Start or install Edubuntu"
msgstr "^Paleisti ar įdiegti Edubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_edubuntu_xforcevesa
msgid "Start Edubuntu in safe ^graphics mode"
msgstr "Paleisti Edubuntu VESA ^grafiniu režimu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_xubuntu
msgid "^Start or install Xubuntu"
msgstr "^Paleisti ar įdiegti Xubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_xubuntu_xforcevesa
msgid "Start Xubuntu in safe ^graphics mode"
msgstr "Paleisti Xubuntu VESA ^grafiniu režimu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_driverupdates
msgid "Install with driver ^update CD"
msgstr "Įdiegti naudojant ^valdyklių atnaujinimo CD"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text
msgid "^Install in text mode"
msgstr "Į^diegti tekstinėje aplinkoje"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install
msgid "^Install to the hard disk"
msgstr "Į^diegti į standųjį diską"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_workstation
#, fuzzy
msgid "Install a ^workstation"
msgstr "Įdiegti ^darbo vietos kompiuterį"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_server
msgid "Install a ser^ver"
msgstr "Įdiegti ser^verį"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_oem
msgid "^OEM install (for manufacturers)"
msgstr "^OEM įdiegimas (kompiuterių gami^ntojams)"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_lamp
msgid "Install a ^LAMP server"
msgstr "Įdiegti ^LAMP serverį"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_ltsp
msgid "Install an ^LTSP server"
msgstr "Įdiegti ^LTSP serverį"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_cli
msgid "Install a comm^and-line system"
msgstr "Įdiegti sistemą su ^komandinės eilutės aplinka"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_check
msgid "^Check CD for defects"
msgstr "^Patikrinti, ar nėra CD defektų"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_rescue
msgid "^Rescue a broken system"
msgstr "Iš^gelbėti sugadintą sistemą"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_memtest
msgid "^Memory test"
msgstr "^Operatyviosios atminties (RAM) tikrinimas"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_hd
msgid "^Boot from first hard disk"
msgstr "Į^kelti iš pirmojo standaus disko"
